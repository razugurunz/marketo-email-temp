###################################################
Info about files and folders inside of downloaded folder
###################################################

1. markup.zip 
>> Please ignore this zipped folder.

2. index.html
>> Markup without marketo variables added.

3. index-with-marketo-variables.html
>> Markup with marketo variables added. Please use this file while making templates in marketo dashboard.

Looking forward to work with you!